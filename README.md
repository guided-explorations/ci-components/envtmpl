# Ultimate Automated SemVersioning - Next Version Determination with GitVersion

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/ci-components/checkov-iac-sast)

## Usage

### Original Docmentation

[go envtmpl docs](https://github.com/williambailey/go-envtmpl/blob/master/README.md)

### Inputs and Configuration

| Input | Default value |    Type     | Description |
| ----- | ------------- | ----------- | ----------- |
| `CI_DEBUG_TRACE` | N/A | CI Variable     | Causes verbose component output for debugging. Set for pipeline or specific job. |
| `SOURCEFILEPATHLIST` | N/A | CI Variable     | The string to insert into the middle of the Hello World log messages. |
| `STAGE` | .pre | CI Comp Input     | The CI Stage to run the component in. |

### IMPORTANT: Override CI YAML 'artifacts:paths'

Multiple artifact paths cannot be specified in a GitLab CI variable (artifact:paths does not expand the variable and treats it as one variable name.)

You must override artifacts with this (which is also shown in at least one of the working examples below):

```yaml
envtmpl:
  variables:
    SOURCEFILEPATHLIST: 'example.txt.tmpl,config/example2.txt.tmpl'
  artifacts:
    paths: [ example.txt, config/example2.txt ]
```

### Debug Tracing

Set either CI_COMPONENT_TRACE or GitLab's global trace (CI_DEBUG_TRACE) to 'true' to get detailed debugging. More info: https://docs.gitlab.com/ee/ci/variables/#enable-debug-logging"

### Including

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: $CI_SERVER_FQDN/guided-explorations/ci-components/envtmpl/envtmpl@<VERSION>
```
See "Components" tab in CI Catalog to copy exactly pointing to the current version.

where `<VERSION>` is the latest released tag or `main`.

### Original Canonical Source Code

The original source for this component is at: https://gitlab.com/guided-explorations/ci-components/checkov-iac-sast

### Validation Test

If you simply include the component and every job after it will have the following pipeline variables that used the infamous GitVersion to create a unique SemVer according
to complex uniqueness rules that are configurable by you in the GitVersion.yml file.

### Working Example Code Using This Component

- [envtmpl](https://gitlab.com/guided-explorations/ci-components/working-code-examples/envtmpl-test) - notice the main .gitlab-ci.yml overrides `artifacts:paths` in the named job for the component.

## Contribute

- [CI/CD Components Documentation](https://docs.gitlab.com/ee/ci/components)
- [Darwin's CI Component Builders Guide](https://gitlab.com/guided-explorations/ci-components/gitlab-profile/-/blob/main/README.md)